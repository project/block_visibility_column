<?php

namespace Drupal\block_visibility_column\Controller;

use Drupal\block\BlockListBuilder;
use Drupal\block\Entity\Block;

/**
 * Custom list builder.
 */
class BlockVisibilityColumnListBuilder extends BlockListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildBlocksForm() {
    $entities = $this->load();

    $form = parent::buildBlocksForm();
    array_splice($form['#header'], 4, 0, [$this->t('Visibility')]);
    foreach ($form as $key => &$element) {
      if (isset($element['operations'])) {
        $op = array_pop($element);
        $element['visibility'] = ['#markup' => implode('<br>', $this->getVisibility($entities[$key]))];
        $element['operations'] = $op;
      }
    }
    return $form;
  }

  /**
   * Get visibility.
   */
  private function getVisibility(Block $block): array {
    $v = $block->getVisibility();
    $t = [
      'user_role' => 'roles',
      'language' => 'langcodes',
      'request_path' => 'pages',
      'entity_bundle:taxonomy_term' => 'bundles',
      'entity_bundle:node' => 'bundles',
      'token_matcher' => [
        'token_match',
        'check_empty',
        'value_match',
        'use_regex',
      ],
    ];

    if (!empty($v)) {
      foreach ($v as &$item) {
        if (!in_array($item['id'], array_keys($t))) {
          $item = $item['id'] . ' is not supported';
          continue;
        }
        $sign = $item['negate'] ? ' <> ' : ' = ';
        if ($item['id'] === 'token_matcher') {
          $value = [];
          foreach ($t[$item['id']] as $element) {
            $value[$element] = $element . '=' . (is_bool($item[$element]) ?
                ($item[$element] ? 'TRUE' : 'FALSE') : $item[$element]);
          }
        }
        else {
          $value = $item[$t[$item['id']]];
        }
        if (is_array($value)) {
          $value = implode(', ', $value);
        }
        else {
          $value = str_replace([
            '<',
            '>',
            "\r\n",
          ], [
            '&lt;',
            '&gt;',
            ', ',
          ], $value);
        }
        $item = $item['id'] . $sign . $value;
      }
    }
    return $v;
  }

}
